I created this little demo. To learn swift and demonstrate my coding style in it.

It parses a basic news feed  Hacker News Api to show the top and new stories.

Details of the stories can be viewed by pressing on any table view Entry.

Objective C and swift are designed to run parallel.
To run Obj C code in swift a Bridge file is created where you can put your Obj C class headers to be used in swift.