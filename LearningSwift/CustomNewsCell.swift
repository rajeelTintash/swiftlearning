//
//  CustomNewsCell.swift
//  LearningSwift
//
//  Created by Rajeel Amjad on 03/07/2015.
//  Copyright (c) 2015 Tintash. All rights reserved.
//
import Alamofire
import UIKit


class CustomNewsCell: UITableViewCell {
    
     @IBOutlet weak var newsTitleLabel: UILabel?
     @IBOutlet weak var newsAuthorLabel: UILabel?
    
    var dataDict: NSMutableDictionary = NSMutableDictionary()
    func setCellData(newsDetailObject:HackerNewsDetailObject){
        

        self.newsTitleLabel?.text = newsDetailObject.title    // set title for cell
        self.newsAuthorLabel?.text = newsDetailObject.author  // set author in gray label
//        println(id.objectForKey("title") as! String)
        
//        var error:NSError
//        var urlString = "https://hacker-news.firebaseio.com/v0/item/"
//        urlString = urlString+id 
//        urlString = urlString + ".json?print=pretty"
//        println(urlString)
//        Alamofire.request(.GET, urlString).responseJSON() {
//            (_, _, JSON, _) in
//            //            println(JSON)
//             self.dataDict = (JSON! as! NSMutableDictionary)
//            println(self.dataDict)
////            self.newsArray = (JSON! as! NSMutableArray)
//        }
        
        // First tried calling object detail Api from with in cell. Which is wrong approach obviously but was trying to see what can be achieved.

        
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}