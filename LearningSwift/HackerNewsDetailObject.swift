//
//  HackerNewsDetailObject.swift
//  LearningSwift
//
//  Created by Rajeel Amjad on 03/07/2015.
//  Copyright (c) 2015 Tintash. All rights reserved.
//

import UIKit

class HackerNewsDetailObject: NSObject {
    var title:String?
    var url:String?
    var author:String?
    
    // Only Needed 3 Elements so only those in need saved in object
    
    func initWithWithDictionary(dict:NSDictionary) {
        self.title = dict.objectForKey("title") as? String
        self.url = dict.objectForKey("url") as? String
        self.author = dict.objectForKey("by") as? String
        
    }

}
