//
//  Constants.swift
//  LearningSwift
//
//  Created by Rajeel Amjad on 03/07/2015.
//  Copyright (c) 2015 Tintash. All rights reserved.
//



struct Constants {
    static let BASE_URL = "https://hacker-news.firebaseio.com/v0/"
    static let NEWS_STORIES = "newstories.json?"
    static let TOP_STORIES = "topstories.json?"
}