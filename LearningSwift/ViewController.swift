//
//  ViewController.swift
//  LearningSwift
//
//  Created by Rajeel Amjad on 03/07/2015.
//  Copyright (c) 2015 Tintash. All rights reserved.
//
import Alamofire
import UIKit


class ViewController: UIViewController {

    @IBOutlet weak var newsTableView : UITableView?
    var newsArray: NSMutableArray = NSMutableArray()
    var isTopStories : Bool = false
    var refreshControl:UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newsTableView?.scrollsToTop = true   // pressing the status bar scrolls to top
        self.title = "New Stories"
        var b = UIBarButtonItem(title: "Top Stories", style: .Plain, target: self, action: "toggleNewsFeed")
        self.navigationItem.rightBarButtonItem = b
        
        self.refreshControl = UIRefreshControl()   // add pull down to refresh
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.newsTableView?.addSubview(self.refreshControl)
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
        // fetch the news
        self.fetchNews()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Mark JSON request
    
    func refresh() {
        // for UI refresh control could use same fetchNews Method for it but wanted to create seperate as I was In learning swift.
        self.fetchNews()
        
    }
    func toggleNewsFeed() {
        
        // To Toggle between Top Stories and New Stories
        self.isTopStories = !self.isTopStories
        if (self.isTopStories) {
            var b = self.navigationItem.rightBarButtonItem;
            b!.title = "New Stories"
//            var b = UIBarButtonItem(title: "New Stories", style: .Plain, target: self, action: "toggleNewsFeed")
//            self.navigationItem.rightBarButtonItem = b
            self.title = "Top Stories"
        }
        else {
            var b = self.navigationItem.rightBarButtonItem;
            b!.title = "Top Stories"
//            var b = UIBarButtonItem(title: "Top Stories", style: .Plain, target: self, action: "toggleNewsFeed")
//            self.navigationItem.rightBarButtonItem = b
            self.title = "New Stories"
        }
        self.fetchNews()
    }
    func fetchNews () {
        
        var urlString : String!
        // get URL for current selected mde Top stories or New Stories
        if (isTopStories) {
            urlString = Constants.BASE_URL+Constants.TOP_STORIES
        }
        else {
            urlString = Constants.BASE_URL+Constants.NEWS_STORIES
        }
        
        
        Alamofire.request(.GET, urlString).responseJSON() {
            (_, _, JSON, _) in
            //            println(JSON)
            let tempNewsArray = (JSON! as! NSMutableArray) // Get Id's of all news Items
           // println(tempNewsArray)
            self.newsArray.removeAllObjects()
            self.newsTableView?.reloadData()
            self.refreshControl?.endRefreshing()
            for newsID in tempNewsArray {   // fetching details
                var urlString = Constants.BASE_URL+"item/"
                urlString = urlString+newsID.stringValue
                urlString = urlString + ".json?print=pretty"
                //                println(urlString)
                Alamofire.request(.GET, urlString).responseJSON() {
                    (_, _, JSON, _) in
                    
                    //            println(JSON)
                    if JSON != nil {
                        let dataDict = (JSON! as! NSMutableDictionary)  // adding to custom model object
                        //                    println(dataDict)
                        var tempNewsDetailObject : HackerNewsDetailObject = HackerNewsDetailObject()
                        tempNewsDetailObject.initWithWithDictionary(dataDict)
                        self.newsArray.addObject(tempNewsDetailObject)
                        self.newsTableView?.reloadData()
                    }
                    //            self.newsArray = (JSON! as! NSMutableArray)
                }
                
            }
            //            self.newsTableView?.reloadData()
        }
    }
    
    //Mark : Tableview
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return CGFloat
//    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("customNewsCell" , forIndexPath: indexPath) as! CustomNewsCell
        
        let row = indexPath.row
//        println("Hello")
//        if var tempcell = cell{
            cell.setCellData(newsArray.objectAtIndex(indexPath.row) as! HackerNewsDetailObject)  // populate the cell
//        }
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let newsDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NewsDetailWebView") as! NewsDetailWebView
//        var tempUrlString = self.newsArray.objectAtIndex(indexPath.row).objectForKey("url") as! String
//        println(tempUrlString)
        
        // Push New view Controller
        newsDetailViewController.currentObject = (self.newsArray.objectAtIndex(indexPath.row) as! HackerNewsDetailObject)
        self.navigationController?.pushViewController(newsDetailViewController, animated: true)
    }
    
    
    
    


}

