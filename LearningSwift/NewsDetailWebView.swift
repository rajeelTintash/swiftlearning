//
//  NewsDetailWebView.swift
//  LearningSwift
//
//  Created by Rajeel Amjad on 03/07/2015.
//  Copyright (c) 2015 Tintash. All rights reserved.
//

import UIKit

class NewsDetailWebView: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var newsDetailWebView : UIWebView?
    var currentObject:HackerNewsDetailObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.currentObject?.title
//        println(self.currentObject?.url)
        var tempUrlString = self.currentObject?.url
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)  // Addin Mb Progress Hud i.e Loading
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = "Loading"
        self.newsDetailWebView?.loadRequest(NSURLRequest(URL: NSURL(string:tempUrlString!)!))  // Load Webview
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark : WebView Delegate
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        print("Webview fail with error \(error)");
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        // remove hud on fail to load so ui unfreezes
        
    }
    
    
    
    func webViewDidStartLoad(webView: UIWebView) {
        print("Webview started Loading")
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        print("Webview did finish load")
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        // remove hud on page load
    }

}
